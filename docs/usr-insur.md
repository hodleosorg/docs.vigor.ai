---
id: usr-insur
title: How to be an insurer
sidebar_label: Deposit Insurance
---
Cryptos and VIGOR stablecoin are deposited into the insurance pool to earn VIG and to insure the system against both upside and downside market stress events. Insurers agree to accept their share of bailouts (automatically get assigned ownership of failed collateral and associated debt) according to their contribution to solvency (PCTS).

## HOW TO ADD INSURANCE VIA DEMO INTERFACE
* Find the **Insurance** box
* The box shows the amount of insurance tokens already present (if any) and their value 
* Select the token from **drop down menu** 
* Fill the field with the desired amount
* Click on **DEPOSIT**
* Confirm Scatter pop-up

## HOW TO ADD INSURANCE VIA BLOKS.IO
* Go to: **bloks.io > wallet > Transfer token**
* Send To: **vigordemo111**
* Amount: **CHOOSE YOUR AMOUNT OF EOS, IQ, BOID OR VIGOR**
* Memo: **insurance**

## Please note
* Your insurance assets are shown in the **"Insurance"** column of the contract
* After deposit you will start earning some VIGs over time. They will appear in the same **"Insurance"** column of the contract

The columns representing the variables in the contract table can be found here:

**bloks.io > vigordemo111 > Contract > Tables > User**



