---
id: usr-borr-stable
title: Borrow VIGOR stablecoin
sidebar_label: Borrow stablecoin
---
## HOW TO BORROW VIGOR STABLECOIN VIA DEMO INTERFACE
* Find the **Debt** box
* The box shows the amount of VIGOR tokens already borrowed (if any)
* Fill the field with the desired amount
* Click on **BORROW**
* Confirm Scatter pop-up


## HOW TO BORROW VIGOR STABLECOIN VIA BLOKS.IO
* Go to: **bloks.io > vigordemo111 > Contract > Actions > assetout**
* usern: *YOURUSERNAME*
* assetout: *THE AMOUNT OF VIGOR YOU WANT TO BORROW*
* Memo: **borrow**
* Click on **Submit Transaction**
* Confirm Scatter pop-up