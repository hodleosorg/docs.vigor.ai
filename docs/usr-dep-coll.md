---
id: usr-dep-coll
title: Deposit collateral
sidebar_label: Deposit collateral
---

## HOW TO ADD COLLATERAL VIA DEMO INTERFACE
* Find the **Collateral** box
* The box shows the amount of collateral tokens already present (if any) and their value 
* Select the token from **drop down menu** 
* Fill the field with the desired amount
* Click on **DEPOSIT**
* Confirm Scatter pop-up


## HOW TO ADD COLLATERAL VIA BLOKS.IO
* Go to: **bloks.io > wallet > Transfer token**
* Send To: **vigordemo111**
* Amount: **CHOOSE YOUR AMOUNT OF EOS, IQ, BOID OR VIGOR**
* Memo: **collateral**

## Please note
You can only deposit accepted collateral tokens which for now are: EOS, IQ, BOID and VIGOR.
* If you deposit EOS, IQ or BOID, they will appear in the **collateral** column of the contract.
* If you deposit VIGOR, they will appear in the **l_debt** column of the contract.

The columns representing the variables in the contract table can be found here:

**bloks.io > vigordemo111 > Contract > Tables > User**