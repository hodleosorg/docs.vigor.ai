---
id: usr-borr-token
title: Borrow EOS tokens
sidebar_label: Borrow EOS tokens
---
## HOW TO BORROW EOS VIA DEMO INTERFACE
* Find the **Debt** box
* The box shows the amount of EOS tokens already borrowed (if any)
* Fill the field with the desired amout
* Click on **BORROW**
* Confirm Scatter pop-up


## HOW TO BORROW EOS VIA BLOKS.IO
* Go to: **bloks.io > vigordemo111 > Contract > Actions > assetout**
* usern: *YOURUSERNAME*
* assetout: *THE AMOUNT OF EOS YOU WANT TO BORROW*
* Memo: **borrow**
* Click on **Submit Transaction**
* Confirm Scatter pop-up

## Please note
Collateral must always exceed your total borrowings by 1.11