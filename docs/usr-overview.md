---
id: usr-overview
title: Vigor Jungle Testnet Tutorial
sidebar_label: Interact with the contract
---
**Currently testing on vigordemo111 contract**

## HOW TO INTERACT WITH THE CONTRACT
* There are two ways to interact with the contract: 
    * through the **Vigor Demo Interface** (the easiest way)
    * through **bloks.io** 



## INTERACT USING VIGOR DEMO INTERFACE
* To interact with the contract through the demo interface go to: [https://vigordemo.netlify.com/](https://vigordemo.netlify.com/)
*  Click on **Login**
*  Login with Scatter paying attention to select your Jungle Testnet Account
    * If this is your first interaction with the contract, first register your account 
    * Click on **Open Account**
    * Confirm Scatter pop-up



## INTERACT USING VIGOR CONTRACT ON BLOKS.IO
* To interact with the contract go to [https://jungle.bloks.io/](https://jungle.bloks.io/)
* Login with Scatter paying attention to select Jungle Testnet
* Head to your wallet at: [https://jungle.bloks.io/wallet/transfer](https://jungle.bloks.io/wallet/transfer)
    * The **Transfer Token** tab is what you will need
* In another tab open the contract page: [https://jungle.bloks.io/account/vigordemo111](https://jungle.bloks.io/account/vigordemo111)  
    * If this is your first interaction with the contract, first register your account 
    * Click on **</> Contract** tab
    * I the **Actions** tab select **openaccount**
    * In **Owner** type your account name
    * Click **Submit transaction**
    * Your account has been created on the new contract
## Please Note
Most interactions on Bloks.io will take place through the **Actions** tab
* Use the **Tables** tab to explore general contract statistics 
* **Tables > user** contains a list of all users with their stats, including you

This is all you need to interact with the contract using Bloks.io