---
id: dev-actions
title: Actions
sidebar_label: Actions
---


*  **assetouth** - When a user executed **assetout**, the action **assetouth** is also executed. However this includes a 5 seconds delay to prevent front running (the oracle data is lagged). **assetout** does a bunch of checks. If these pass, it then executes an asynchronous call to **assetouth**, the delay is the deferred transaction part
