---
id: usr-get-start
title: Vigor Jungle Testnet Tutorial
sidebar_label: Getting Started
---



## WHAT YOU NEED FOR THE TEST
* An **account on Jungle Testnet** to login into the demo interface or contract on bloks.io
* A few **Jungle EOS** 
* (optional) you can **setup Scatter** for a faster and easier login


## HOW TO CREATE AN ACCOUNT ON TESTNET
*  Go to [http://monitor.jungletestnet.io](http://monitor.jungletestnet.io/)
*  Click on **Create Key**
    * copy/save you pair 
*  Click on **Create Account**
    * Choose an **Account name** with 12 characters (a-z,1-5)
    *  Paste your keys
    *  Verify **reCaptcha**
    *  **Create**

## HOW TO GET JUNGLE EOS
*  Go to [http://monitor.jungletestnet.io](http://monitor.jungletestnet.io/)
    * Click on **Faucet**
    * Write your **Account name**
    * Verify **reCaptcha**
    * Click **Send Coins** (100 EOS will be credited to your account)
    * (don’t be scared by the red text. Everything is OK.)
    * Come back after 6 hours to claim more free Jungle EOS
* You will also find anoter **Faucet** button when you will log into the demo interface.

## HOW TO SET UP SCATTER (optional)
**(Scatter v.11.x)**
* Download **Scatter** from [https://get-scatter.com/download](https://get-scatter.com/download)
* Open **Scatter**
* Go to **Administrative > Networks > Add custom network**
    * Find Network Api list : [http://monitor.jungletestnet.io/#apiendpoints](http://monitor.jungletestnet.io/#apiendpoints)
    * In **Network** tab > 
    * Name: put a name of your choice, like "**Jungle2 Testnet**”
    * Host: **jungle2.cryptolions.io**
    * Protocol: **https**
    * Port: **443**
    * Chain ID: **e70aaab8997e1dfce58fbfac80cbbb8fecec7b99cf982a9444273cbc64c41473**
    * **System Token** tab >
    * Switch **ON** the button
    * Contract: **eosio.token**
    * Symbol: **EOS**
    * Decimals: **4**
    * **Save new network**
* Go to **Wallet > Import Key**
* Select **Text** and **paste** your **private key**
* Back to Wallet
* Find your new network name and click on the drop-down menu near the key icon.
* While the drop-down menu is open, **press CTRL** to reveal the hidden function.
* Select **Link Account**
* Type your **account name** and check that Jungle Teestnet is correctly selected
* Press **Link Account**
* Scatter configuration is done.

